<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Warung PandanSari</title>
<!--

Template 2081 Solution

http://www.tooplate.com/view/2081-solution

-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Sigmar+One" rel="stylesheet">
		<!-- custom -->
		<link rel="stylesheet" href="css/style.css">
		<!--slick theme-->
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/slick-theme.css">
		<!-- DataTables -->
	  <link rel="stylesheet" href="css/dataTables.bootstrap.css">

	</head>
	<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><h1 id="logo"><span>W</span>arung <span>P</span>andansari</h1></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" id="nav-menu">
						<li><a href="index.php">PANDANSARI</a></li>
						<li><a href="#">LAYANAN</a></li>
						<li><a href="menu.php">MENU</a></li>
						<li><a href="#">FASILITAS</a></li>
						<li><a href="#" class="nav-active">HUBUNGI</a></li>
						<li><a href="galeri.php">GALERI</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

		<div class="main-content">

      <!-- start service -->
      <div id="menu-contact">
      	<div class="container">
      		<div class="row">
      			 <div class="col-md-6 col-sm-6">
      				 <h3><span>K</span>ontak</h3>
      				 <p class="sub-tit">Nomor telpon:</p>
      				 <p><i class="fa fa-phone"></i> 085-106-790-101 (Warung Pandansari)</p>
      				 <p><i class="fa fa-phone"></i> 081-23-386-385 (H.Ilham Robby)</p>
      				 <p><i class="fa fa-phone"></i> 081-233-107-107 (Bu Hari)</p>
      				 <p><i class="fa fa-phone"></i> 081-333-293-837 (Bu Amah)</p>
      				 <p class="sub-tit">Alamat Email:</p>
      				 <p><i class="fa fa-envelope"></i> agustine.indah28@gmail.com</p>
      				 <p class="sub-tit">BBM:</p>
      				 <p><i class="fa fa-comment"></i> 53DA71F3</p>
      				 <p class="sub-tit">Lokasi:</p>
      				 <p><i class="fa fa-map-marker"></i> Kami berlokasi di jalan utama desa Pandesari kecamatan Pujon, dekat dengan Kota Wisata Batu, sehingga kami juga dekat dengan berbagai tempat wisata seperti Jatim Park, Museum Angkut, Museum Satwa, Batu Secret Zoo, Coban Rondo, Paralayang, Pemandian Songgoriti, Pemandian Dewi Sri, Selecta, Agrowisata, BNS, Museum Tubuh, Alun-alun Batu Anda dapat mengunjungi kami di Jl. Brigjend Abdul Manan W. no. 70 Pandesari, Pujon, Malang, Jawa Timur</p>
      			 </div>
      			 <div class="col-md-6 col-sm-6">
      				 <h3><span>M</span>ap</h3>
      				 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.4108657143347!2d112.48305121415522!3d-7.851995694342512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78877c7b0fd235%3A0xf06eddb69258c2d6!2sRumah+Makan+Pandansari!5e0!3m2!1sid!2sid!4v1479991819768" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      			 </div>
      		 </div>
      	</div>
      </div>

		</div>
		<!--end main-content-->

		<div style="text-align:center;margin-bottom:1em;">
			<p>
				<h3>Tetap terhubung</h3>
			</p>
		</div>

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row social-icon wow fadeInUp" data-wow-delay="0.9s">
					<div class="col-md-12 col-sm-12">

					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-facebook fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-twitter fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-instagram fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-pinterest fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-google fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-youtube fa-2x"></a>
					</div>
				</div>
				<div class="copyright">
					<p>Copyright &copy; <span>Warung Pandansari</span></p>
				</div>
			</div>
		</footer>
		<!-- end footer -->

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- isotope -->
		<script src="js/isotope.js"></script>
		<!-- images loaded -->
   	<script src="js/imagesloaded.min.js"></script>
   		<!-- wow -->
		<script src="js/wow.min.js"></script>
		<!-- jquery flexslider -->
		<script src="js/jquery.flexslider.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
    <!--slick-->
  	</body>
</html>
