<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Warung PandanSari</title>
<!--

Template 2081 Solution

http://www.tooplate.com/view/2081-solution

-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Sigmar+One" rel="stylesheet">
		<!-- custom -->
		<link rel="stylesheet" href="css/style.css">
		<!--slick theme-->
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/slick-theme.css">
		<!-- DataTables -->
	  <link rel="stylesheet" href="css/dataTables.bootstrap.css">

	</head>
	<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><h1 id="logo"><span>W</span>arung <span>P</span>andansari</h1></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" id="nav-menu">
						<li><a href="index.php">PANDANSARI</a></li>
						<li><a href="#">LAYANAN</a></li>
						<li><a href="#" class="nav-active">MENU</a></li>
						<li><a href="#">FASILITAS</a></li>
						<li><a href="contact.php">HUBUNGI</a></li>
						<li><a href="galeri.php">GALERI</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

		<div class="main-content">

      <!-- start service -->
      <div id="menu-list" style="margin-top:7em;">
      	<div class="container">
      		<div class="slid slider" id="category-list">
      			<div style="margin:1em;text-align:center;"><h3><span>M</span>akanan</h3></div>
      			<div style="margin:1em;text-align:center;"><h3><span>M</span>inuman</h3></div>
      			<div style="margin:1em;text-align:center;"><h3><span>P</span>aket <span>P</span>rasmanan</h3></div>
      			<div style="margin:1em;text-align:center;"><h3><span>P</span>aket <span>N</span>asi <span>K</span>otak</h3></div>
      		</div>
      		<div class="data-menu">
      			<div id="table-menu-1">
      				<table>
      					<thead>
      						<th colspan="2" style="background:transparent;"><img src="images/menu.jpg" style="width:100%;eight:150px;"></th>
      					</thead>
      				</table>
      				<table class="table table-striped table-menu">
      					<thead>
      						<th><h3>Menu Makanan</h3></th>
      						<th><h3>Keterangan</h3></th>
      					</thead>
      					<tbody>
      						<tr>
      							<td>Ayam Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Ati empela atau usus</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Kepala Ayam</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      					</tbody>
      					<tfoot>
      						<tr>
      							<td colspan="2"></td>
      						</tr>
      					</tfoot>
      				</table>
      			</div>
      			<div id="table-menu-2">
							<table>
      					<thead>
      						<th colspan="2" style="background:transparent;"><img src="images/menu_2.jpg" style="width:100%;eight:150px;"></th>
      					</thead>
      				</table>
      				<table class="table table-striped table-menu">
      					<thead>
      						<th><h3>Menu Minuman</h3></th>
      						<th><h3>Keterangan</h3></th>
      					</thead>
      					<tbody>
      						<tr>
      							<td>Ayam Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Ati empela atau usus</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Kepala Ayam</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      					</tbody>
      					<tfoot>
      						<tr>
      							<td colspan="2"></td>
      						</tr>
      					</tfoot>
      				</table>
						</div>
      			<div id="table-menu-3">
							<table>
      					<thead>
      						<th colspan="2" style="background:transparent;"><img src="images/menu_3.jpg" style="width:100%;eight:150px;"></th>
      					</thead>
      				</table>
      				<table class="table table-striped table-menu">
      					<thead>
      						<th><h3>Paket Prasmanan</h3></th>
      						<th><h3>Keterangan</h3></th>
      					</thead>
      					<tbody>
      						<tr>
      							<td>Ayam Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Ati empela atau usus</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Kepala Ayam</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      					</tbody>
      					<tfoot>
      						<tr>
      							<td colspan="2"></td>
      						</tr>
      					</tfoot>
      				</table>
						</div>
      			<div id="table-menu-4">
							<table>
      					<thead>
      						<th colspan="2" style="background:transparent;"><img src="images/menu_4.jpg" style="width:100%;eight:150px;"></th>
      					</thead>
      				</table>
      				<table class="table table-striped table-menu">
      					<thead>
      						<th><h3>Paket Nasi Kotak</h3></th>
      						<th><h3>Keterangan</h3></th>
      					</thead>
      					<tbody>
      						<tr>
      							<td>Ayam Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Ati empela atau usus</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Kepala Ayam</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      						<tr>
      							<td>Nasi Goreng</td>
      							<td>Goreng</td>
      						</tr>
      					</tbody>
      					<tfoot>
      						<tr>
      							<td colspan="2"></td>
      						</tr>
      					</tfoot>
      				</table>
						</div>
      		</div>
      	</div>
      </div>
      <!-- end service -->
		</div>
		<!--end main-content-->

		<div style="text-align:center;margin-bottom:1em;">
			<p>
				<h3>Tetap terhubung</h3>
			</p>
		</div>

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row social-icon wow fadeInUp" data-wow-delay="0.9s">
					<div class="col-md-12 col-sm-12">

					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-facebook fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-twitter fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-instagram fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-pinterest fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-google fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-youtube fa-2x"></a>
					</div>
				</div>
				<div class="copyright">
					<p>Copyright &copy; <span>Warung Pandansari</span></p>
				</div>
			</div>
		</footer>
		<!-- end footer -->

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- isotope -->
		<script src="js/isotope.js"></script>
		<!-- images loaded -->
   	<script src="js/imagesloaded.min.js"></script>
   		<!-- wow -->
		<script src="js/wow.min.js"></script>
		<!-- jquery flexslider -->
		<script src="js/jquery.flexslider.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
    <!--slick-->
    <script src="js/slick.min.js"></script>
    <!-- DataTables -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <!--function-->
    <script>
      $(document).ready(function(){
      $('.data-menu').slick({
      	slidesToShow: 1,
      	slidesToScroll: 1,
      	arrows: false,
      	fade: true,
      	asNavFor: '.slid'
      });

      $('.slid').slick({
      	centerMode: true,
      	centerPadding: '60px',
      	slidesToShow: 3,
      	asNavFor: '.data-menu',
      	responsive: [
      		{
      			breakpoint: 468,
      			settings: {
      				arrows: false,
      				centerMode: true,
      				centerPadding: '40px',
      				slidesToShow: 3
      			}
      		},
      		{
      			breakpoint: 480,
      			settings: {
      				arrows: false,
      				centerMode: true,
      				centerPadding: '40px',
      				slidesToShow: 1
      			}
      		}
      	]
      });
      });

      $(document).ready(function(){
      $(".table-menu").DataTable({
      	language: {
        paginate: {
            previous: '‹',
            next:     '›'
        },
        aria: {
            paginate: {
                previous: 'Previous',
                next:     'Next'
            }
        }
      },
      	"pageLength": 5,
      	"paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
      });
      });
    </script>
	</body>
</html>
